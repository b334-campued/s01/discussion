package com.zuitt.example;

// A 'package' in Java is used to group related classes. Think of it as a folder in a file directory
//
// Package creation in Java follows the 'reverse domain name notation' for the naming convention

/*

In Java, it's a convention to use the "reverse domain name notation" when naming packages.
This convention helps ensure that package names are unique and avoids naming conflicts when
different developers or organizations create libraries or applications.

Here's how the "reverse domain name notation" works:
1. Start with your domain name in reverse order. For example, if your domain is "example.com,"
you would start with "com.example."

2. Append any additional package names specific to your project or organization.
For instance, if your project is called "myproject," you would add it as "com.example.myproject."

3. Each part of the package name is separated by dots, just like the regular domain notation.
*/

// Packages are divided into two categories:
    //Built-in Packages from Java API
    //User-defined Packages (Create your own packages)


public class Variables {

        public  static  void  main (String[] args){
            //Naming Convention
                //All identifiers/variables should begin with a letter, $ , _ BUT not a number
                //After the first character can have any combination of characters
                // Identifiers are case sensitive

                //Variable Declaration:
                int age;
                char middleName;

                //Variable Declaration with Initialization:
                int x;
                int y = 0;

                //Initialization
                x = 1;

                //output to the system:
                System.out.println("The value of y is " + y + " and the value of x is " + x);

                //Primitive Data types:
                        //Predefined within the Java which is used for single valued variable with limited capabilities

                // int - whole numbers
                int wholeNumber = 10000;
                System.out.println(wholeNumber);

                // long - L is added to the end of the long number to be recognized
                long worldPopulation = 454218942424619L;
                System.out.println(worldPopulation);

                //Floating value
                //float add f at the end of the value
                //up to 7 decimal places and rounded if exceed
                float piFloat = 3.14151612424242f;
                System.out.println("The value of piFloat is " + piFloat);


                //double - floating values
                double piDouble = 3.14151612424242;
                System.out.println("The value of piDouble is " + piDouble);

                //char = single character
                //uses single quote
                char letter = 'a';
                System.out.println(letter);

                //boolean - true or false
                boolean isLove = true;
                boolean isTaken = false;
                System.out.println(isLove);
                System.out.println(isTaken);


                //constants where value cannot be reassigned
                // Java uses the 'final' keyword so that variables value cannot be changed (like const)
                // As best practice usually named in UPPERCASE

                final int PRINCIPAL = 3000;
                System.out.println(PRINCIPAL);


                // Non-Primitive Data
                        // also known as 'reference' data types refers to instances or objects
                        // does not directly store the value of the variable, but rather remember the reference to the variable

                // String
                        //Stores a sequence or array of characters
                        // String are actually object that can use methods (That's why you can use .length in JS)
                        // Uses double quote?
                String username = "JSmith";
                System.out.println(username);
                int stringLength = username.length();
                System.out.println(stringLength);

        }



}
