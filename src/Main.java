// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    // public access modifier which simply tells application which classes have access to this method/attribute
    // static-keyword associated with method/property that is related in class. This will allow a method to be invoked without instantiating a class.
    // This means no need to instantiate the class (create object from class) then use/call a method
    // which means no need to invoke to run/call a method
    // void-keyword that is used to specify a method that doesn't return any value.
    // In Java, we have to declare the data type of methods return not just only identified/variable declaration

    //String[] args accepts a single argument of the type String array that contains commandline argument

    public static void main(String[] args) {
        // Press Alt+Enter with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        System.out.println("Hello and welcome!");

        // Press Shift+F10 or click the green arrow button in the gutter to run the code.
        for (int i = 1; i <= 5; i++) {

            // Press Shift+F9 to start debugging your code. We have set one breakpoint
            // for you, but you can always add more by pressing Ctrl+F8.
            System.out.println("i = " + i);
        }
    }
}


// Summary

// Access modifier (public)
// Retrieval (yes)
// Modification (yes)
// Inheritance (yes)
// Retrieval of Subclass (yes)
// Modification of Subclass (yes)

// Access modifier (private)
// Retrieval (no) - use getter
// Modification (no) - use setter and then getter for update
// Inheritance (no)
//Retrieval of Subclass (no)	- use getter
// Modification of Subclass (yes) - SURPRISINGLY(new property); You can still use setter but for update to show use getter

// Access modifier (protected)
// Retrieval (no)  - use getter
//Modification (no)	- use setter and then getter for update
//Inheritance (yes)
//Retrieval of Subclass (no)	- use getter
//Modification of Subclass (no) - use setter and then getter for update